###########################################################################
# Dockerfile to build the Using Drupal 2e Book's source code on LAMP stack
###########################################################################

# Set the base image to PHP-Apache
FROM php:5-apache

# File Author / Maintainer
MAINTAINER John Walker, bitbucket.org/johnniewalkeruk

# Set the servername conf to suppress Apache warnings
# See: http://askubuntu.com/a/396048/28555

 # Put the servername conf in place
 COPY config/apache2/servername.conf /etc/apache2/conf-available/servername.conf

 #Enable that conf
 RUN a2enconf servername

# Copy the custom PHP.ini 
COPY config/php.ini /usr/local/etc/php/php.ini

# Move the Using Drupal Source Code from our webdocs
# to the appropriate place
COPY webdocs/ /var/www/html/

# Ensure that the files directory can be modified by Drupal's installation script
RUN mkdir /var/www/html/drupal-7/sites/default/files
RUN chmod a+w /var/www/html/drupal-7/sites/default/files

# Drupal Settings file
# Drupal will try to automatically create a settings.php configuration file,
# which is normally in the directory sites/default (to avoid problems when
# upgrading, Drupal is not packaged with this file). If auto-creation fails,
# you will need to create this file yourself, using the file
# sites/default/default.settings.php as a template.

# make a copy of the default.settings.php file with the command:

RUN cp /var/www/html/drupal-7/sites/default/default.settings.php /var/www/html/drupal-7/sites/default/settings.php

# Next, grant write privileges to the file to everyone (including the web server) with the command:
RUN chmod a+w /var/www/html/drupal-7/sites/default/settings.php

# Be sure to set the permissions back after the installation is finished!
# Sample command:
# chmod go-w sites/default/settings.php



# Install modules that Drupal Core requires

 # Gd Library. 
 # This is based on the Extensions example in the Docker-php page. 
 # See: https://registry.hub.docker.com/_/php/
 RUN apt-get update && apt-get install -y \
        libfreetype6-dev \
        libjpeg62-turbo-dev \
        libpng12-dev \
    && docker-php-ext-configure gd --with-freetype-dir=/usr/include/ --with-jpeg-dir=/usr/include/ \
    && docker-php-ext-install gd

# Install recommended extensions for Drupal

 #  Install the PHP mbstring extension for improved Unicode support.
 RUN docker-php-ext-install mbstring

# Install DB.

 #Mysql server

 # We have to preset some answers cos installation asks us for root password:
 # See: http://stackoverflow.com/a/20037235/205814

 RUN apt-get update \
	&& echo "mysql-server mysql-server/root_password password root" | debconf-set-selections \
	&& echo "mysql-server mysql-server/root_password_again password root" | debconf-set-selections \
	&& apt-get install -y \
 	mysql-server \
	mysql-client
 

 # Copy the mysql schema creation files
 COPY config/db/set-up-dbs.sql /tmp/set-up-dbs.sql

 # Start the mysql server and the set-up code
 RUN service mysql start \
	&& mysql -uroot -proot < /tmp/set-up-dbs.sql

 # Ensure mysql is set to start upon bootup
 # See: http://askubuntu.com/a/2265/28555
 RUN update-rc.d mysql defaults



# The Drupal requirements docs say pdo version on mysql is fine.
# I based this on example code at: https://github.com/docker-library/php/blob/master/docker-php-ext-install#L7
RUN docker-php-ext-install pdo_mysql

# Now ensure that mysql is started upon CMD by appending to the apache2-foreground command in the base image.
# See: https://github.com/docker-library/php/blob/master/5.4/apache/apache2-foreground
# I added a line to it to start mysql as well
COPY config/docker-cmd/apache2-foreground /usr/local/bin/apache2-foreground

# We need to chmod it to make it executable again so it comes out as: 
# -rwxr-xr-x 1 root root 132 Dec 19 22:23 /usr/local/bin/apache2-foreground
RUN chmod 755 /usr/local/bin/apache2-foreground

# Let the base image issue the final CMD to start mysql and exec apache.
