[TOC]
# README 

## About this repository

This Bitbucket repository contains: 

* the source code for the [Using Drupal 2nd Edition](https://www.waterstones.com/book/using-drupal/heather-berry/addison-berry/9781449390525) book (which I cloned from `https://usingdrupal.unfuddle.com/git/usingdrupal_usingdrupalsource7`). See the [book's website](http://usingdrupal.com/source_code): 
* some files that enable us to run Drupal as a Docker container

These files will help you get an instance of Drupal up-and-running quite easily so you can start working through the *Using Drupal* book with very little trouble.

Note: It is, currently, about £70 cheaper to buy The Using Drupal 2nd Edition paperback from [Waterstones (£29.99)](https://www.waterstones.com/book/using-drupal/heather-berry/addison-berry/9781449390525) than [Amazon (£99.99)](http://www.amazon.co.uk/dp/B00E27YHY2/sr=8-4/qid=1446506771/).

## Requirements

We assume that you have a 64bit Virtual Machine running Linux with [Docker](https://www.docker.com/) installed on it (Docker requires a 64-bit machine). 

## Install the source onto a Linux VM

* Start running your Linux Virtual Machine (I use VMware).

* Clone the repo to a directory, say, `/path/to/using-drupal7-book`

* Build the Docker image

```
$ cd /path/to/using-drupal7-book
$ sudo docker build -t my-php-app .
```

## Run the container

Mapping port 80 to port 80:

```
$ sudo docker run -it --rm -p 80:80 --name my-running-app my-php-app
```

## Determine the IP address to visit

Type ifconfig to determine the IP Address of the VMware VM


```
$ ifconfig
eth0      Link encap:Ethernet  HWaddr 00:0c:29:b9:16:8f  
          inet addr:192.168.161.129  Bcast:192.168.161.255  Mask:255.255.255.0
```   

## Visit the web application

Then, on the Win 7 host, start-up the web browser and visit:

```
http://192.168.161.129
```

## Install Drupal

Use the following details:

### Install Profile:

* Database type: `MySql`
* Database name: `using_drupal_2e_chapter1` (or whatever chapter you are on see config/db/set-up-dbs.sql for info)
* Database username: `drupal`
* Database password: `drupal`
* Advanced options
    * Database host: `127.0.0.1`
    * Database port: `` (empty)
    * Table prefix: `` (empty) 

### Configure Site:
* Site information:
    * Site name: `Using Drupal`
    * Site e-mail address: `example@example.com`

* Site Maintenance Account:
    * Username: `admin`
    * E-mail address:  `example@example.com`
    * Password: `admin`

* Server Settings:
    * Default country:  `whatever`
    * Default time zone: `whatever`

* Update Notifications:
    * Check for updates automatically: `Unchecked` (for sanity)


## SSH into the Docker instance 

Open up a new terminal.

Display the list of running docker containers to identify our one:

```
$ sudo docker ps
CONTAINER ID        IMAGE               COMMAND                CREATED             
    STATUS              PORTS                NAMES
d3ba41b2b727        my-php-app:latest   "apache2-foreground"   49 seconds ago      
    Up 49 seconds       0.0.0.0:80->80/tcp   my-running-app
```

Then:

```
$ sudo docker exec -i -t my-running-app bash
```

Type exit to detach from the container and return to the command line.


## To wipe the Drupal installation and re-install 


We need to drop the database and start again.

SSH in to the Docker instance (see above).

Recreate the database using the info that you find in the config/db/set-up-dbs.sql file.

```
$ mysql -uroot -proot < /tmp/set-up-dbs.sql
```

Then re-visit the IP address for the Docker instance.