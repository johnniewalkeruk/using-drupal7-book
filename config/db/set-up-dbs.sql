# uninstall if anything's already there
GRANT ALL PRIVILEGES ON *.* TO 'drupal'@'%';
DROP USER 'drupal'@'%';

DROP DATABASE IF EXISTS `using_drupal_2e_chapter1`;
DROP DATABASE IF EXISTS `using_drupal_2e_chapter2`;
DROP DATABASE IF EXISTS `using_drupal_2e_chapter3`;
DROP DATABASE IF EXISTS `using_drupal_2e_chapter4`;
DROP DATABASE IF EXISTS `using_drupal_2e_chapter5`;
DROP DATABASE IF EXISTS `using_drupal_2e_chapter6`;
DROP DATABASE IF EXISTS `using_drupal_2e_chapter7`;
DROP DATABASE IF EXISTS `using_drupal_2e_chapter8`;
DROP DATABASE IF EXISTS `using_drupal_2e_chapter9`;
DROP DATABASE IF EXISTS `using_drupal_2e_appendixA`;
DROP DATABASE IF EXISTS `using_drupal_2e_appendixB`;
DROP DATABASE IF EXISTS `using_drupal_2e_appendixC`;
DROP DATABASE IF EXISTS `using_drupal_2e_appendixD`;

# create the user
CREATE USER 'drupal'@'%' IDENTIFIED BY 'drupal';

CREATE DATABASE IF NOT EXISTS `using_drupal_2e_chapter1`;
CREATE DATABASE IF NOT EXISTS `using_drupal_2e_chapter2`;
CREATE DATABASE IF NOT EXISTS `using_drupal_2e_chapter3`;
CREATE DATABASE IF NOT EXISTS `using_drupal_2e_chapter4`;
CREATE DATABASE IF NOT EXISTS `using_drupal_2e_chapter5`;
CREATE DATABASE IF NOT EXISTS `using_drupal_2e_chapter6`;
CREATE DATABASE IF NOT EXISTS `using_drupal_2e_chapter7`;
CREATE DATABASE IF NOT EXISTS `using_drupal_2e_chapter8`;
CREATE DATABASE IF NOT EXISTS `using_drupal_2e_chapter9`;
CREATE DATABASE IF NOT EXISTS `using_drupal_2e_appendixA`;
CREATE DATABASE IF NOT EXISTS `using_drupal_2e_appendixB`;
CREATE DATABASE IF NOT EXISTS `using_drupal_2e_appendixC`;
CREATE DATABASE IF NOT EXISTS `using_drupal_2e_appendixD`;


GRANT ALL PRIVILEGES ON `using_drupal_2e_chapter1` . * TO 'drupal'@'%';
GRANT ALL PRIVILEGES ON `using_drupal_2e_chapter2` . * TO 'drupal'@'%';
GRANT ALL PRIVILEGES ON `using_drupal_2e_chapter3` . * TO 'drupal'@'%';
GRANT ALL PRIVILEGES ON `using_drupal_2e_chapter4` . * TO 'drupal'@'%';
GRANT ALL PRIVILEGES ON `using_drupal_2e_chapter5` . * TO 'drupal'@'%';
GRANT ALL PRIVILEGES ON `using_drupal_2e_chapter6` . * TO 'drupal'@'%';
GRANT ALL PRIVILEGES ON `using_drupal_2e_chapter7` . * TO 'drupal'@'%';
GRANT ALL PRIVILEGES ON `using_drupal_2e_chapter8` . * TO 'drupal'@'%';
GRANT ALL PRIVILEGES ON `using_drupal_2e_chapter9` . * TO 'drupal'@'%';
GRANT ALL PRIVILEGES ON `using_drupal_2e_appendixA` . * TO 'drupal'@'%';
GRANT ALL PRIVILEGES ON `using_drupal_2e_appendixB` . * TO 'drupal'@'%';
GRANT ALL PRIVILEGES ON `using_drupal_2e_appendixC` . * TO 'drupal'@'%';
GRANT ALL PRIVILEGES ON `using_drupal_2e_appendixD` . * TO 'drupal'@'%';



